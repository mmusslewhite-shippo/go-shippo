module bitbucket.org/shippotle/go-shippo

go 1.15

require (
	contrib.go.opencensus.io/exporter/prometheus v0.2.0 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/google/go-cmp v0.5.3 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.2 // indirect
	go.opencensus.io v0.22.5 // indirect
	golang.org/x/net v0.0.0-20210316092652-d523dce5a7f4 // indirect
	google.golang.org/grpc v1.33.2 // indirect
)
