package services

import (
	"errors"
	"fmt"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"go.opencensus.io/plugin/ocgrpc"
	"go.opencensus.io/stats/view"
	"google.golang.org/grpc"
	"log"
)

func CreateStatsEnabledGRPCServer() (*grpc.Server, error) {
	recoveryOpts := []grpc_recovery.Option{
		grpc_recovery.WithRecoveryHandler(GRPCPanicHandler),
	}
	grpcServer := grpc.NewServer(
		grpc_middleware.WithUnaryServerChain(
			grpc_recovery.UnaryServerInterceptor(recoveryOpts...),
		),
		grpc.StatsHandler(&ocgrpc.ServerHandler{}),
	)

	err := view.Register(ocgrpc.DefaultServerViews...)

	return grpcServer, err
}

//TODO: have custom metric for number of panics in prometheus
func GRPCPanicHandler(message interface{}) error {
	msg := fmt.Sprintf("Panic recovered!!! Message - %v", message)
	log.Println(msg)
	return errors.New(msg)
}
