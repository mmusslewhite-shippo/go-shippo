package services

import (
	"fmt"
	"net/http"
)

func CreateHealthCheckServer(portNumber int32) *http.Server {
	mux := http.NewServeMux()

	mux.Handle("/robots.txt", &HealthCheckHandler{})

	server := http.Server{
		Addr:    fmt.Sprintf(":%d", portNumber),
		Handler: mux,
	}
	return &server
}

type HealthCheckHandler struct{}

func (h *HealthCheckHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ping ok"))
}
